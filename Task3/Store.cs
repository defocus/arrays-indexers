﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    internal class Store
    {
        Article[] articles = 
            { 
                new Article("Pen","Store_1", 2), 
                new Article("Pencil","Store_2", 2),
                new Article("Mechanical Pencil","Store_1", 5),
                new Article("Marker","Store_2", 5),
                new Article("Elastic","Store_1", 3),
                new Article("Stapler","Store_2", 7),
                new Article("Mechanical pencil lead","Store_1", 3),
                new Article("Pen stem","Store_1", 2),                
                new Article("Colored pencils","Store_2", 10),                
                new Article("Ruler","Store_2", 5),                
            };

        public string this[string nameProduct]
        {
            get
            {
                return SearchArticle(nameProduct);
            }
        }

        private string SearchArticle(string nameProduct)
        {
            for (int i = 0; i < articles.Length; i++)
                if (articles[i].NameProduct == nameProduct)
                    return $"Name Product:\t{articles[i].NameProduct}\n" +
                        $"Store:\t\t{articles[i].StoreName}\n" +
                        $"Price:\t\t{articles[i].Price} UAH\n" +
                        new string('-', 30);

            return $"{nameProduct}: This item was not found";
        }
    }
}
