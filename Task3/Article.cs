﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    internal class Article
    {
        string _nameProduct;
        string _storeName;
        double _price;

        public string NameProduct => _nameProduct;
        public string StoreName => _storeName;
        public double Price => _price;
        
        public Article(string nameProduct, string storeName, double price)
        {
            _nameProduct = nameProduct;
            _storeName = storeName;
            _price = price;
        }
    }
}
