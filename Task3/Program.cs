﻿using System;

namespace Task3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Store store = new Store();

            /*
                new Article("Pen","Store_1", 2), 
                new Article("Pencil","Store_2", 2),
                new Article("Mechanical Pencil","Store_1", 5),
                new Article("Marker","Store_2", 5),
                new Article("Elastic","Store_1", 3),
                new Article("Stapler","Store_2", 7),
                new Article("Mechanical pencil lead","Store_1", 3),
                new Article("Pen stem","Store_1", 2),                
                new Article("Colored pencils","Store_2", 10),                
                new Article("Ruler","Store_2", 5),      
            */
            Console.WriteLine(store["Stapler"]);
            Console.WriteLine(store["Mechanical pencil lead"]);
            Console.WriteLine(store["Mechanical"]);

            //Delay
            Console.ReadKey();
        }
    }
}
