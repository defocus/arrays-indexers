﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    internal class ArrayMethods
    {
        private int[] _array;

        public int searchMaxValue => SearchMaxValue(_array);
        
        public int searchMinValue => SearchMinValue(_array);
        
        public int totalAmount => TotalAmount(_array);        
        
        public double avarageNum => AvarageNum(_array);

        public ArrayMethods(int index)
        {
            _array = new int[index];
            
            FillArray(_array);
        }
        
        int SearchMaxValue(int[] array)
        {
            bool index = true;
            int max = 0;
            
            foreach (var item in array)
            {
                if(index)
                {
                    max = item;
                    index = false;
                }                    
                else if(max < item) max = item;
            }

            return max;
        }

        int SearchMinValue(int[] array)
        {
            bool index = true;
            int min = 0;

            foreach (var item in array)
            {
                if (index)
                {
                    min = item;
                    index = false;
                }
                else if (min > item) min = item;
            }

            return min;
        }

        int TotalAmount(int[] array)
        {
            int sum = 0;

            foreach (var item in array)
            {
                sum += item;
            }

            return sum;
        }

        double AvarageNum(int[] array) => TotalAmount(array)/array.Length;

        public void OddNum()
        {
            foreach (var item in _array)
            {
                if(item % 2 != 0)
                {
                    Console.Write($"{item} ");
                }
            }
        }

        private void FillArray(int[] array)
        {
            Random rand = new Random();

            for(int i = 0; i < array.Length; i++)
            {
                array[i] = rand.Next(-1000, 1001);
            }

            OutputArray(array);
        }

        private void OutputArray(int[] array)
        {
            foreach(var item in array)
            {
                Console.Write($"{item} ");
            }
            
            Console.WriteLine();
        }
    }
}
