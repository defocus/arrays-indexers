﻿using System;

namespace Task1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter array length: ");

            int length = int.Parse(Console.ReadLine());

            ArrayMethods array = new ArrayMethods(length);

            Console.Write(new string('-', 50));

            Console.WriteLine(
                $"\n\nMax value array: {array.searchMaxValue}\n" +
                $"Min value array: {array.searchMinValue}\n" +
                $"Total amount of array: {array.totalAmount}\n" +
                $"Arithmetic mean of an array: {array.avarageNum}\n"
                );

            Console.WriteLine("Odd numbers of an array:");
            array.OddNum();

            Console.ReadKey();
        }
    }
}
