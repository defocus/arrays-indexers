﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    internal class MyMatrix
    {
        readonly string[,] _matrix;

        public MyMatrix(int rows, int columns)
        {
            _matrix = new string[rows, columns];

            FillMatrix(_matrix);
        }

        private void FillMatrix(string[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    matrix[i, j] = $"{i}{j}";
                    
                    Console.Write(matrix[i, j] + "\t");
                }

                Console.WriteLine();
            }

            Console.Write(new string('-', 30) + "\n");
        }

        public void ChildMatrix()
        {
            int columnDimension = _matrix.GetLength(1) - 2;
            int rowDimension = _matrix.GetLength(0) - 2;

            for (int m = CountDimension(); m > 0; m--) //Количество возможных порядков матриц
            {
                for (int column = 0; column <= columnDimension; column++) // Два цикла для прохода по возмодным матрицам текущего порядка
                {
                    for (int row = 0; row <= rowDimension; row++)
                    {
                        for (int i = row; i < _matrix.GetLength(0) - rowDimension + row; i++) //Два цикла для вывода на экран матрицы текущего порядка
                        {
                            for (int j = column; j < _matrix.GetLength(1) - columnDimension + column; j++)
                            {
                                Console.Write(_matrix[i, j] + "\t");
                            }

                            Console.WriteLine();
                        }

                        Console.WriteLine(new string('-', 30) + "\n");
                    }
                }

                rowDimension--;
                columnDimension--;
            }
        }

        private int CountDimension()
        {
            if (_matrix.GetLength(0) > _matrix.GetLength(1))
                return _matrix.GetLength(0);

            else return _matrix.GetLength(1);
        }
    }
}
