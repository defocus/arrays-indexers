﻿using System;

namespace Task2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter num rows of array: ");

            int rows = int.Parse(Console.ReadLine());

            Console.Write("Enter num columns of array: ");

            int columns = int.Parse(Console.ReadLine());

            MyMatrix matrix = new MyMatrix(rows, columns);

            matrix.ChildMatrix();
        }
    }
}
